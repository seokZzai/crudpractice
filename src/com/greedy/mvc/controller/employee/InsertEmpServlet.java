package com.greedy.mvc.controller.employee;

import java.io.IOException;
import java.sql.Date;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.greedy.mvc.controller.model.dto.EmployeeDTO;
import com.greedy.mvc.controller.model.service.EmployeeService;


@WebServlet("/employee/insert")
public class InsertEmpServlet extends HttpServlet {
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		request.setCharacterEncoding("UTF-8");
		
		String empName = request.getParameter("empName");
		String empNo = request.getParameter("empNo");
		String email = request.getParameter("email");
		String phone = request.getParameter("phone");
		String deptCode = request.getParameter("deptCode");
		String jobCode = request.getParameter("jobCode");
		String salLevel = request.getParameter("salLevel");
		int salary = Integer.parseInt(request.getParameter("salary"));
		double bonus = Double.parseDouble(request.getParameter("bonus"));
		String managerId = request.getParameter("managerId");
		Date hireDate = Date.valueOf(request.getParameter("hireDate"));
		
		EmployeeDTO edto = new EmployeeDTO();
		edto.setEmpName(empName);
		edto.setEmpNo(empNo);
		edto.setEmail(email);
		edto.setPhone(phone);
		edto.setDeptCode(deptCode);
		edto.setJobCode(jobCode);
		edto.setSalLevel(salLevel);
		edto.setSalary(salary);
		edto.setBonus(bonus);
		edto.setManagerId(managerId);
		edto.setHireDate(hireDate);
		
		EmployeeService empService = new EmployeeService();
		int result = empService.insertEmp(edto);
		
		String path = "";
		if(result > 0) {
			path = "/WEB-INF/views/common/successPage.jsp";
			request.setAttribute("successCode", "insertEmp");
		} else {
			path = "/WEB-INF/views/common/errorPage.jsp";
			request.setAttribute("message", "직원 등록 실패!");
		}
		request.getRequestDispatcher(path).forward(request, response);
	}

}
