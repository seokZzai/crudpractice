package com.greedy.mvc.controller.employee;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.greedy.mvc.controller.model.dto.EmployeeDTO;
import com.greedy.mvc.controller.model.service.EmployeeService;


@WebServlet("/employee/list")
public class SelectAllEmpServlet extends HttpServlet {
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		request.setCharacterEncoding("UTF-8");
		
		EmployeeService empService = new EmployeeService();
		
		List<EmployeeDTO> empList = empService.selectAllEmp();
		
		String path = "";
		if(empList != null) {
			path = "/WEB-INF/views/employee/employeeList.jsp";
			request.setAttribute("empList", empList);
		} else {
			path = "/WEB-INF/views/common/errorPage.jsp";
			request.setAttribute("message", "전체 회원 조회 실패!");
		}
		request.getRequestDispatcher(path).forward(request, response);
	}

}
