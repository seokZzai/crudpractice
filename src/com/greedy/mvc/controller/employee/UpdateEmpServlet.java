package com.greedy.mvc.controller.employee;

import java.io.IOException;
import java.sql.Date;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.greedy.mvc.controller.model.dto.EmployeeDTO;
import com.greedy.mvc.controller.model.service.EmployeeService;


@WebServlet("/employee/update")
public class UpdateEmpServlet extends HttpServlet {
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		request.setCharacterEncoding("UTF-8");
		
		String empId = request.getParameter("empId");
		Date entDate = Date.valueOf(request.getParameter("entDate"));
		
		EmployeeService empService = new EmployeeService();
		int result = empService.updateEmp(empId, entDate);
		
		String path = "";
		if(result > 0) {
			path = "/WEB-INF/views/common/successPage.jsp";
			request.setAttribute("successCode", "updateEmp");
		} else {
			path = "/WEB-INF/views/common/errorPage.jsp";
			request.setAttribute("message", "직원 정보 수정 실패!");
		}
		request.getRequestDispatcher(path).forward(request, response);
	}

}
