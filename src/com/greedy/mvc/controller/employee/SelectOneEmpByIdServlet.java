package com.greedy.mvc.controller.employee;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.greedy.mvc.controller.model.dto.EmployeeDTO;
import com.greedy.mvc.controller.model.service.EmployeeService;

@WebServlet("/employee/select")
public class SelectOneEmpByIdServlet extends HttpServlet {
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		request.setCharacterEncoding("UTF-8");
		
		String empId = request.getParameter("empId");
		
		EmployeeService empService = new EmployeeService();
		EmployeeDTO emp = empService.selectOneEmpById(empId);
		
		String path = "";
		if(emp != null) {
			path = "/WEB-INF/views/employee/showEmpInfo.jsp";
			request.setAttribute("emp", emp);
		} else {
			path = "/WEB-INF/views/common/errorPage.jsp";
			request.setAttribute("message", "직원정보 조회 실패!");
		}
		
		request.getRequestDispatcher(path).forward(request, response);
	}

}
