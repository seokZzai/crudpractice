package com.greedy.mvc.controller.model.service;

import static com.greedy.mvc.common.jdbc.JDBCTemplate.close;
import static com.greedy.mvc.common.jdbc.JDBCTemplate.commit;
import static com.greedy.mvc.common.jdbc.JDBCTemplate.getConnection;
import static com.greedy.mvc.common.jdbc.JDBCTemplate.rollback;

import java.sql.Connection;
import java.sql.Date;
import java.util.List;

import com.greedy.mvc.controller.model.dao.EmployeeDAO;
import com.greedy.mvc.controller.model.dto.EmployeeDTO;

public class EmployeeService {

	private EmployeeDAO edao = new EmployeeDAO();
	
	public EmployeeDTO selectOneEmpById(String empId) {
		
		Connection con = getConnection();
		
		EmployeeDTO emp = edao.selectemp(con, empId);
		
		close(con);
		
		return emp;
	}

	public List<EmployeeDTO> selectAllEmp() {
		
		Connection con = getConnection();
		
		List<EmployeeDTO> empList = edao.selectAllEmp(con);
		
		close(con);
		
		return empList;
	}

	public int insertEmp(EmployeeDTO edto) {
		
		Connection con = getConnection();
		
		int result = edao.insertNewEmp(con, edto);
		
		if(result > 0) {
			commit(con);
		} else {
			rollback(con);
		}
		
		close(con);
		
		return result;
	}

	public int updateEmp(String empId, Date entDate) {
		
		Connection con = getConnection();
		
		int result = edao.updateEmp(con, empId, entDate);
		
		if(result > 0) {
			commit(con);
		} else {
			rollback(con);
		}
		
		close(con);
		
		return result;
	}

	public int deleteEmp(String empId) {
		
		Connection con = getConnection();
		
		int result = edao.deleteEmp(con, empId);
		
		if(result > 0) {
			commit(con);
		} else {
			rollback(con);
		}
		
		close(con);
		
		return result;
	}

}
