package com.greedy.mvc.controller.model.dao;

import static com.greedy.mvc.common.jdbc.JDBCTemplate.close;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import com.greedy.mvc.common.config.ConfigLocation;
import com.greedy.mvc.controller.model.dto.EmployeeDTO;

public class EmployeeDAO {

	private Properties prop = new Properties();
	
	public EmployeeDAO() {
		try {
			prop.loadFromXML(new FileInputStream(ConfigLocation.MAPPER_LOCATION + "employee-mapper.xml"));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public EmployeeDTO selectemp(Connection con, String empId) {
		
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		
		EmployeeDTO edto = null;
		
		String query = prop.getProperty("selectEmpById");
		try {
			pstmt = con.prepareStatement(query);
			
			pstmt.setString(1, empId);
			
			rset = pstmt.executeQuery();
			if(rset.next()) {
				edto = new EmployeeDTO();
				edto.setEmpId(rset.getString("EMP_ID"));
				edto.setEmpName(rset.getString("EMP_NAME"));
				edto.setDeptCode(rset.getString("DEPT_CODE"));
				edto.setJobCode(rset.getString("JOB_CODE"));
				edto.setSalary(rset.getInt("SALARY"));
				
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}
		
		return edto;
	}

	public List<EmployeeDTO> selectAllEmp(Connection con) {
		
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		
		List<EmployeeDTO> empList = null;
		
		String query = prop.getProperty("selectAllEmp");
		
		try {
			pstmt = con.prepareStatement(query);
			
			rset = pstmt.executeQuery();
			
			empList = new ArrayList<>();
			
			while(rset.next()) {
				EmployeeDTO emp = new EmployeeDTO();
				
				emp.setEmpId(rset.getString("EMP_ID"));
				emp.setEmpName(rset.getString("EMP_NAME"));
				emp.setEmail(rset.getString("EMAIL"));
				emp.setEmpNo(rset.getString("EMP_NO"));
				emp.setPhone(rset.getString("PHONE"));
				emp.setDeptCode(rset.getString("DEPT_CODE"));
				emp.setJobCode(rset.getString("JOB_CODE"));
				emp.setSalLevel(rset.getString("SAL_LEVEL"));
				emp.setSalary(rset.getInt("SALARY"));
				emp.setBonus(rset.getDouble("BONUS"));
				emp.setManagerId(rset.getString("MANAGER_ID"));
				emp.setHireDate(rset.getDate("HIRE_DATE"));
				emp.setEntDate(rset.getDate("ENT_DATE"));
				emp.setEntYn(rset.getString("ENT_YN"));
				
				empList.add(emp);
				
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
			
		}
		
		
		return empList;
	}

	public int insertNewEmp(Connection con, EmployeeDTO edto) {
		
		PreparedStatement pstmt = null;
		int result = 0;
		
		String query = prop.getProperty("insertEmp");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, edto.getEmpName());
			pstmt.setString(2, edto.getEmpNo());
			pstmt.setString(3, edto.getEmail());
			pstmt.setString(4, edto.getPhone());
			pstmt.setString(5, edto.getDeptCode());
			pstmt.setString(6, edto.getJobCode());
			pstmt.setString(7, edto.getSalLevel());
			pstmt.setInt(8, edto.getSalary());
			pstmt.setDouble(9, edto.getBonus());
			pstmt.setString(10, edto.getManagerId());
			pstmt.setDate(11, edto.getHireDate());
			
			result = pstmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
		}
		
		return result;
	}

	public int updateEmp(Connection con, String empId, Date entDate) {
		
		PreparedStatement pstmt = null;
		int result = 0;
		
		String query = prop.getProperty("updateEmp");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setDate(1, entDate);
			pstmt.setString(2, empId);
			
			result = pstmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
		}
		
		return result;
	}

	public int deleteEmp(Connection con, String empId) {
		
		PreparedStatement pstmt = null;
		int result = 0;
		
		String query = prop.getProperty("deleteEmp");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, empId);
			
			result = pstmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
		}
		
		return result;
	}

}
